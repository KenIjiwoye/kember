import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('about');
  this.route('projects', function() {
    this.route('new');
    this.route('edit', {path: '/:project_id/edit'});
  });
  this.route('works', function() {
    this.route('batman');
    this.route('gamefly');
    this.route('totalfit');
    this.route('rbp');
    this.route('taskkeeper');
    this.route('chitter');
    this.route('wmc');
    this.route('mak');
    this.route('lipstick-riot');
    this.route('dream-home');
  });
  this.route('lipstick-riot');
});

export default Router;
