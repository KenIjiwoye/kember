import Ember from 'ember';

export default Ember.Route.extend({
	model(params) {
		return this.store.findRecord('project', params.project_id);
	},

	willTransition(transition) {
		let model = this.controller.get('model');

		if (model.get('hasDirtyAttributes')) {
			let confirmation = confirm("Your changes haven't saved yet.");

			if (confirmation) {
				model.rollbackAttributes();
			} else {
				transition.abort();
			}
		}
	}
});
